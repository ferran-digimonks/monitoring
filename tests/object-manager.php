<?php

use App\Kernel;

require __DIR__ . '/../config/bootstrap.php';
$kernel = new Kernel($_SERVER['APP_ENV'], (bool)$_SERVER['APP_DEBUG']);
$kernel->boot();
if ($container = $kernel->getContainer()) {
    return $container->get('doctrine')->getManager();
}

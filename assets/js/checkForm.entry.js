import CheckForm from "./CheckForm";
// any CSS you import will output into a single css file (app.css in this case)
import '../sass/checkForm.scss';

document.addEventListener('DOMContentLoaded', (e) => {
    new CheckForm();
})
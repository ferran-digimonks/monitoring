<?php

namespace App\Command;

use App\Repository\NotificationRepository;
use App\Repository\SiteRepository;
use App\Service\NotificationService;
use App\Service\PingAlertService;
use http\Client\Curl\User;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\ScopingHttpClient;
use Symfony\Component\Yaml\Yaml;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CheckAlertsCommand extends Command
{

    protected static $defaultName = 'check:alerts';

    /**
     * @var PingAlertService
     */
    private $pingAlertService;
    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var NotificationRepository
     */
    private $notificationRepository;
    /**
     * @var NotificationService
     */
    private $notificationService;

    public function __construct(PingAlertService $pingAlertService, SiteRepository $siteRepository, NotificationRepository $notificationRepository, NotificationService $notificationService)
    {
        parent::__construct();
        $this->pingAlertService = $pingAlertService;
        $this->siteRepository = $siteRepository;
        $this->notificationRepository = $notificationRepository;
        $this->notificationService = $notificationService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Checks all the configured alert information from the alerts.yml file and starts sending alerts if necessary')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * Parse the alerts.yml file and pass each AlertSetting instance to the LogAlertService
     * @throws \Exception
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->writeln("Started checking logs, sending alerts if necessary");

        $notifications = $this->notificationRepository->findAll();
        foreach ($notifications as $notification){
            $currentTime = date('H-i');
            $currentTime = str_replace("-", ":", $currentTime);

            $sentAtTime = $notification->getSentAt();
            $sentAtTime = $sentAtTime->format('H:i');

//            if ($currentTime != $sentAtTime){
//                return 0;
//            }

            $this->notificationService->checkNotification($notification);
        }

        if(intval(date('i')) % 5 === 0) { // Check ping every 5 min
            $sites = $this->siteRepository->findAll();
            $io->writeln('PING urls every 5 minutes.');
            foreach ($sites as $site) {
                $this->pingAlertService->checkPing($site->getURL());
            }
        }

        $io->success('Successfully checked all the notifications and websites. Please check ur mailbox');

        return 0;
    }
}
<?php

namespace App\Command;

use App\Service\SSLAlertService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CheckSSLCommand extends Command
{
    protected static $defaultName = 'check:ssl';

    /**
     * @var SSLAlertService
     */
    private $sslAlertService;

    public function __construct(SSLAlertService $sslAlertService)
    {
        parent::__construct();
        $this->sslAlertService = $sslAlertService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Started checking all the SSL certificates of the websites, sending alert if necessary');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->writeln("Started checking SSL certificates, sending alerts if necessary");

        $this->sslAlertService->checkSSLCertificates();

        $io->success('Successfully checked all the SSL Certificates.');

        return 0;
    }
}





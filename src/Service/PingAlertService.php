<?php

namespace App\Service;

use JJG\Ping;

class PingAlertService
{

    /**
     * @var MailService
     */
    private $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function checkPing($url){

        try {
            $ping = new Ping($url);
        } catch (\Exception $e) {
            throw $e;
        }

        $latency = $ping->ping();

        if ($latency !== false) {
            echo 'Latency of host ' . $url . ' is ' . $latency . ' ms' . PHP_EOL;
        } else {
            echo 'Host ' . $url . ' could not be reached' . PHP_EOL;
            $subjectAndMessage = ["[IMMEDIATE ACTION REQUIRED] Failed pinging " . $url, $url . " could not be reached." . PHP_EOL];
            $this->mailService->sendMail($subjectAndMessage);
        }
    }

}
<?php

namespace App\Service;

use App\Entity\Check;
use DateTime;
use Elasticsearch\ClientBuilder;

class ElasticSearchService
{

    /**
     * @var ClientBuilder
     */
    private $client;

    public function __construct()
    {
        $hosts = [
            '165.22.195.110:9200',
        ];
        $this->client = ClientBuilder::create()->setHosts($hosts)->build();
    }

    /**
     * @param Check $check
     * Function to query elastic checks
     */
    public function queryElastic(Check $check): string
    {
        $currentDate = date('Y-m-d');
        $currentDate = str_replace('-', '.', $currentDate);
        $indexName = $check->getElasticIndex() . $currentDate;

        $timeSpanHours = $check->getTimeSpan();
        $formattedTimeSpanHours = $timeSpanHours->format('H');

        $timeSpanDate = new DateTime();
        $timeSpanDate->modify('-' . $formattedTimeSpanHours . ' hours');
        $timeSpanDate = $timeSpanDate->format("Y-m-d\TH:i:s\Z");

        // Check if the index exists
        $indexParams['index'] = $indexName;
        if ($this->client->indices()->exists($indexParams) == 0){
            $indexNotFoundError = "ERROR, Index: " . $indexName . " does not exist in the Elasticsearch cluster";
            return "<div style='padding: 15px;background: #f8d7da; border-color: #f5c6cb; color: #721c24; border-radius: .25rem; margin: 10px 0 ;'>{$indexNotFoundError}</div>";
        }

        $params = $this->getParams($check, $timeSpanDate, $indexName);
        $response = $this->client->search($params);
        $amountOfLogs = $response['hits']['total'];

        if (!$check->isValid($amountOfLogs)) {
            return $this->getFailureMessage($check, $amountOfLogs, $formattedTimeSpanHours);
        }

        $successMessage = "The \"" . $check->getDescription() . "\" check went successful." . PHP_EOL;

        return "<div style='padding: 15px;background: #d4edda; border-color: #c3e6cb; color: #155724; border-radius: .25rem; margin: 10px 0 ;'>{$successMessage}</div>";
    }

    private function getParams(Check $check, $timeSpanDate, $indexName): array
    {
        if ($check->getMessage() != null) {
            $messageOrLevel = ['message', $check->getMessage()];
        } else {
            $messageOrLevel = ['level', $check->getLevel()];
        }

        return [
            'index' => $indexName,
            'type' => 'doc',
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'match' => [
                                    $messageOrLevel[0] => $messageOrLevel[1]
                                ]
                            ],
                            [
                                'range' => [
                                    '@timestamp' => [
                                        'gte' => $timeSpanDate,  //gte = great than or equal
                                        'format' => "yyyy-MM-dd||yyyy-MM-dd'T'HH:mm:ss'Z'",
                                        'boost' => 2.0
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ]
        ];
    }

    private function getFailureMessage(Check $check, int $amountOfLogs, int $timeSpanHours): string
    {

        $message = "The \"" . $check->getDescription() . "\" check has failed." . PHP_EOL;

        if (!empty($check->getLevel() && $check->getMessage() == null)) {
            $message .= "The level \"" . $check->getLevel() . "\" has been logged " . $amountOfLogs . " times in the past " . $timeSpanHours . " hour(s) ";
        }

        if (!empty($check->getMessage() && $check->getLevel() == "none")) {
            $message .= "The message \"" . $check->getMessage() . "\" has been logged " . $amountOfLogs . " times in the past " . $timeSpanHours . " hour(s) ";
        }

        if ($check->getMin() != null && $check->getMax() != null) {

            $message .= "and was not within the min/max range of " . $check->getMin() . "/" . $check->getMax() . "." . PHP_EOL;

        } else if ($check->getMin() != null) {

            $message .= "and did not exceed the minimum threshold of " . $check->getMin() . ".". PHP_EOL;

        } else if ($check->getMax() != null) {

            $message .= "and exceeds the maximum threshold of " . $check->getMax() . "." . PHP_EOL;

        } else {
            return "No message result.. Something went wrong";
        }

        return "<div style='padding: 15px;background: #f8d7da; border-color: #f5c6cb; color: #721c24; border-radius: .25rem; margin: 10px 0 ;'>{$message}</div>";

    }

    /**
     * @return ClientBuilder
     */
    public function getClient()
    {
        return $this->client;
    }
}
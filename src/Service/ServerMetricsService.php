<?php

namespace App\Service;

use App\Entity\Check;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ServerMetricsService
{

    const CENTRAL_LOGGING_API = "455a166416db029013012ee7417f58b2f6f90cb45a2ab97c1962cf08879241e7";
    const CENTRAL_LOGGING_DROPLET_ID = 178546058;
    const URL = "https://api.digitalocean.com/v2/droplets/" . self::CENTRAL_LOGGING_DROPLET_ID;

    // const URL = "https://api.digitalocean.com/v2/droplets";
    // const DROPLET_IP = "165.22.195.110";

    public function __construct()
    {
    }

    public function checkServerMetrics(Check $check): string {

        $client = HttpCLient::create();

        try {
            $response = $client->request('GET', self::URL, [
                'headers' => [
                    'content-type' => 'application/json',
                    'Authorization' => 'Bearer ' . self::CENTRAL_LOGGING_API,
                ]
            ]);
        } catch (TransportExceptionInterface $e) {
            echo $e;
        }

        echo $response->getContent();

        return "";
    }

}
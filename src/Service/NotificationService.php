<?php

namespace App\Service;

use App\Entity\Check;
use App\Entity\Notification;

class NotificationService
{
    /**
     * @var ElasticSearchService
     */
    private $elasticSearchService;
    /**
     * @var MailService
     */
    private $mailService;
    /**
     * @var ServerMetricsService
     */
    private $serverMetricsService;

    public function __construct(ElasticSearchService $elasticSearchService, ServerMetricsService $serverMetricsService, MailService $mailService)
    {
        $this->elasticSearchService = $elasticSearchService;
        $this->mailService = $mailService;
        $this->serverMetricsService = $serverMetricsService;
    }

    public function checkNotification(Notification $notification){

        $checks = $notification->getChecks();

        $body = $notification->getBodyIntro() . PHP_EOL . PHP_EOL;

        // Go through each check and append the result to the notification body
        foreach ($checks as $check){

            if ($check->getType() == "elastic_log") {
               $body .= $this->elasticSearchService->queryElastic($check);
            }

            else if ($check->getType() == "server_metrics_cpu_avg"){
                // Get server metrics cpu
                $body .= $this->serverMetricsService->checkServerMetrics($check);
            }
        }

//        echo "Notification body " . $body;
        // Send notification with the complete body from all the check results
        $this->mailService->sendMail(null, $notification, $body);

    }


}
<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Spatie\SslCertificate\SslCertificate;
use App\Repository\SiteRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * Class SSLAlertService
 * @package App\Service
 * checks all the SSL certificates of the Site entities and sends an alert if the expiring date is less than 7 days
 */
class SSLAlertService
{

    /**
     * @var SiteRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var MailService
     */
    private $mailService;

    public function __construct(SiteRepository $repository, MailService $mailService, EntityManagerInterface $entityManager)
    {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
        $this->mailService = $mailService;
    }

    public function checkSSLCertificates()
    {
        $sites = $this->repository->findAll();

        foreach ($sites as $site) {
            $certificate = SslCertificate::createForHostName($site->getURL());
            $site->setDaysLeft($certificate->expirationDate()->diffInDays());

            if ($certificate->expirationDate()->diffInDays() > 7) {
                $site->setAlerting(false);
                $this->entityManager->persist($site);
                continue;
            }

            // Do not alert again if alerting already is true
            if ($site->getAlerting()) {
                continue;
            }

            $subjectAndMessage = ['[IMMEDIATE ACTION REQUIRED] ' . $site->getURL() . ' certificate is almost expired', 'Only ' . $certificate->expirationDate()->diffInDays() . ' days left until the SSL certificate of the domain ' . $site->getURL() . ' is expiring. Renew the SSL certificate as soon as possible'];
            $this->mailService->sendMail($subjectAndMessage);
        }

        $this->entityManager->flush();
    }

}
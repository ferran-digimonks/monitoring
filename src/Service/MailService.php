<?php


namespace App\Service;

use App\Entity\Notification;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class MailService
{

    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendMail(array $subjectAndMessage = null, Notification $notification = null, string $body = null)
    {
        if ($notification != null) {
            $recipients = explode(",", $notification->getRecipients());

            $firstRecipient = array_shift($recipients);

            $email = (new TemplatedEmail())
                ->htmlTemplate('mails/log-mailing.html.twig')
                ->from('ferran@digimonks.com')
                ->to($firstRecipient)
                ->subject($notification->getSubject())
                ->context(
                    ['body' => nl2br($body)]
                );

            if (count($recipients) > 0) {
                foreach ($recipients as $recipient) {
                    $email->addTo($recipient);
                }
            }

        } else {
            $subject = $subjectAndMessage[0];
            $message = $subjectAndMessage[1];
            $email = (new Email())
                ->from('ferran@digimonks.com')
                ->to('ferran@digimonks.com')
                ->subject($subject)
                ->html($message);
        }
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            echo $e;
        }
    }
}
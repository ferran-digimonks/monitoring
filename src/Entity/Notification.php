<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PascalDeVink\ShortUuid\ShortUuid;
use DigiMonks\SymfonyTenants\Doctrine\Annotation\PermissionAware;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 * @PermissionAware()
 */
class Notification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="guid")
     */
    private $guid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subject;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $recipients;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $bodyIntro;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $createdBy;

    /**c
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Check", inversedBy="notification")
     */
    private $checks;

    /**
     * @ORM\Column(type="time")
     */
    private $sentAt;

    public function __construct()
    {
        $this->guid = ShortUuid::uuid4();
        $this->checks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getRecipients(): ?string
    {
        return $this->recipients;
    }

    public function setRecipients(?string $recipients): self
    {
        $this->recipients = $recipients;

        return $this;
    }

    public function getBodyIntro(): ?string
    {
        return $this->bodyIntro;
    }

    public function setBodyIntro(?string $body): self
    {
        $this->bodyIntro = $body;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|Check[]
     */
    public function getChecks(): Collection
    {
        return $this->checks;
    }

    public function addCheck(Check $checkCondition): self
    {
        if (!$this->checks->contains($checkCondition)) {
            $this->checks[] = $checkCondition;
        }

        return $this;
    }

    public function removeCheck(Check $checkCondition): self
    {
        if ($this->checks->contains($checkCondition)) {
            $this->checks->removeElement($checkCondition);
        }

        return $this;
    }


    public function getSentAt(): ?\DateTimeInterface
    {
        return $this->sentAt;
    }

    public function setSentAt(\DateTimeInterface $sentAt): self
    {
        $this->sentAt = $sentAt;

        return $this;
    }
}

<?php

namespace App\Entity;

use DigiMonks\SymfonyTenants\Doctrine\Annotation\PermissionAware;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SiteRepository")
 * @PermissionAware()
 */
class Site
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $URL;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $alerting;

    /**
     * @ORM\Column(type="guid")
     */
    private $guid;

    /**
     * @ORM\Column(type="integer")
     */
    private $daysLeft;

    public function getId(): int
    {
        return $this->id;
    }

    public function getURL(): string
    {
        return $this->URL;
    }

    public function setURL(string $URL): self
    {
        $this->URL = $URL;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAlerting(): ?bool
    {
        return $this->alerting;
    }

    public function setAlerting(bool $alerting): self
    {
        $this->alerting = $alerting;

        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getDaysLeft(): ?int
    {
        return $this->daysLeft;
    }

    public function setDaysLeft(int $daysLeft): self
    {
        $this->daysLeft = $daysLeft;

        return $this;
    }
}

<?php

namespace App\Entity;

use Cassandra\Time;
use DigiMonks\SymfonyTenants\Doctrine\Annotation\PermissionAware;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PascalDeVink\ShortUuid\ShortUuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CheckRepository")
 * @PermissionAware()
 * @ORM\Table("`check`")
 */
class Check
{

    const TYPE_SERVER_METRICS_CPU_AVG = 'server_metrics_cpu_avg';
    const TYPE_SERVER_METRICS_DISK_USAGE_PERCENT = 'server_metrics_disk_usage_percent';
    // ie 26GB
    const TYPE_SERVER_METRICS_DISK_USAGE = 'server_metrics_disk_usage';
    const TYPE_ELASTIC_LOG = 'elastic_log';

    const TYPES = [
        'Server Metrics: cpu avg' => self::TYPE_SERVER_METRICS_CPU_AVG,
        'Server Metrics: disk usage %' => self::TYPE_SERVER_METRICS_DISK_USAGE_PERCENT,
        'Server Metrics: disk usage' => self::TYPE_SERVER_METRICS_DISK_USAGE,
        'Elastic Log' => self::TYPE_ELASTIC_LOG
    ];

    const NONE = 'none';
    const ALERT = 'alert';
    const ERROR = 'error';
    const INFO = 'info';
    const CRITICAL = 'critical';
    const DEBUG = 'debug';
    const NOTICE = 'notice';
    const LOG = 'log';
    const EMERGENCY = 'emergency';
    const WARNING = 'warning';

    const LEVELS = [
        'None' => self::NONE,
        'Alert' => self::ALERT,
        'Error' => self::ERROR,
        'Info' => self::INFO,
        'Critical' => self::CRITICAL,
        'Debug' => self::DEBUG,
        'Notice' => self::NOTICE,
        'Log' => self::LOG,
        'Emergency' => self::EMERGENCY,
        'Warning' => self::WARNING
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="guid")
     */
    private $guid;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $min;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $max;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $timeSpan;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $level;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Notification", mappedBy="checkCondition")
     */
    private $notifications;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $elasticIndex;

    
    public function __construct()
    {
        $this->guid = ShortUuid::uuid4();
        $this->notifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setMin(?int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function setMax(?int $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function getTimeSpan(): ?\DateTimeInterface
    {
        return $this->timeSpan;
    }

    public function setTimeSpan(\DateTimeInterface $timeSpan): self
    {
        $this->timeSpan = $timeSpan;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setLevel(?string $level): self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotifications(Notification $notifications): self
    {
//        if (!$this->notifications->contains($notification)) {
//            $this->notifications[] = $notification;
//            $notification->addCheckCondition($this);
//        }

        return $this;
    }

    public function removeNotifications(Notification $notifications): self
    {
        if ($this->notifications->contains($notifications)) {
            $this->notifications->removeElement($notifications);
            $notifications->removeCheckCondition($this);
        }

        return $this;
    }

    public function getElasticIndex(): ?string
    {
        return $this->elasticIndex;
    }

    public function setElasticIndex(?string $elasticIndex): self
    {
        $this->elasticIndex = $elasticIndex;

        return $this;
    }

    public function isValid(int $amount): bool {

        if ($this->getMin() != null){
            if($amount < $this->min) {
                return false;
            }
        }

        if ($this->getMax() != null){
            if ($amount > $this->max) {
                return false;
            }
        }

        return true;
    }

}

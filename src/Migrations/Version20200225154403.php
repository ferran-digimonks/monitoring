<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200225154403 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, name VARCHAR(255) NOT NULL, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL, phone VARCHAR(100) DEFAULT NULL, avatar VARCHAR(100) DEFAULT NULL, locale VARCHAR(255) DEFAULT \'nl\' NOT NULL, admin_theme VARCHAR(25) DEFAULT \'dm\' NOT NULL, last_login_at DATETIME DEFAULT NULL, last_activity_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, guid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D6492B6FCFB2 (guid), INDEX IDX_8D93D649DE12AB56 (created_by), INDEX IDX_8D93D64916FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tenant_member (id INT AUTO_INCREMENT NOT NULL, guid VARCHAR(24) NOT NULL, tenant_id INT NOT NULL, user_id INT NOT NULL, UNIQUE INDEX UNIQ_514F9E212B6FCFB2 (guid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tenant_member_role (tenant_member_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_7314CCBA566F1223 (tenant_member_id), INDEX IDX_7314CCBAD60322AC (role_id), PRIMARY KEY(tenant_member_id, role_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, guid VARCHAR(24) NOT NULL, level INT NOT NULL, slug VARCHAR(255) NOT NULL, is_tenant_role TINYINT(1) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tenant (id INT AUTO_INCREMENT NOT NULL, guid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', subdomain VARCHAR(50) NOT NULL, company_name VARCHAR(100) NOT NULL, logo VARCHAR(100) DEFAULT NULL, phone VARCHAR(40) DEFAULT NULL, email VARCHAR(100) DEFAULT NULL, website VARCHAR(100) DEFAULT NULL, suspended TINYINT(1) NOT NULL, `database` VARCHAR(50) DEFAULT NULL, created_by INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_4E59C4622B6FCFB2 (guid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_permission (id INT AUTO_INCREMENT NOT NULL, role_id INT NOT NULL, permission_id INT NOT NULL, guid VARCHAR(32) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permission (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, guid VARCHAR(24) NOT NULL, UNIQUE INDEX UNIQ_E04992AA2B6FCFB2 (guid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE translation (id INT AUTO_INCREMENT NOT NULL, guid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', name VARCHAR(255) NOT NULL, translation LONGTEXT NOT NULL, locale VARCHAR(20) NOT NULL, domain VARCHAR(75) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE revision (id INT AUTO_INCREMENT NOT NULL, created_by_id INT DEFAULT NULL, guid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, entity VARCHAR(50) NOT NULL, entity_id VARCHAR(50) NOT NULL, previous_state JSON NOT NULL, new_state JSON NOT NULL, action VARCHAR(50) NOT NULL, version INT DEFAULT NULL, INDEX IDX_6D6315CCB03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE password_reset (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, guid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', token VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_B1017252A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mail_template (id INT AUTO_INCREMENT NOT NULL, guid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', slug VARCHAR(100) NOT NULL, subject VARCHAR(255) NOT NULL, css_path VARCHAR(100) DEFAULT NULL, body LONGTEXT NOT NULL, tenant_id INT NOT NULL, layout VARCHAR(100) NOT NULL, cc VARCHAR(255) DEFAULT NULL, bcc VARCHAR(255) DEFAULT NULL, created_by INT NOT NULL, updated_by INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_4AB7DECB989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64916FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tenant_member_role ADD CONSTRAINT FK_7314CCBA566F1223 FOREIGN KEY (tenant_member_id) REFERENCES tenant_member (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tenant_member_role ADD CONSTRAINT FK_7314CCBAD60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE revision ADD CONSTRAINT FK_6D6315CCB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE password_reset ADD CONSTRAINT FK_B1017252A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649DE12AB56');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64916FE72E1');
        $this->addSql('ALTER TABLE revision DROP FOREIGN KEY FK_6D6315CCB03A8386');
        $this->addSql('ALTER TABLE password_reset DROP FOREIGN KEY FK_B1017252A76ED395');
        $this->addSql('ALTER TABLE tenant_member_role DROP FOREIGN KEY FK_7314CCBA566F1223');
        $this->addSql('ALTER TABLE tenant_member_role DROP FOREIGN KEY FK_7314CCBAD60322AC');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE tenant_member');
        $this->addSql('DROP TABLE tenant_member_role');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE tenant');
        $this->addSql('DROP TABLE role_permission');
        $this->addSql('DROP TABLE permission');
        $this->addSql('DROP TABLE translation');
        $this->addSql('DROP TABLE revision');
        $this->addSql('DROP TABLE password_reset');
        $this->addSql('DROP TABLE mail_template');
    }
}

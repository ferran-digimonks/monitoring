<?php

namespace App\Presenter;

use App\Entity\Site;
use DigiMonks\DataTables\Column;
use DigiMonks\DataTables\DataTable;
use DigiMonks\DataTables\DataTableQueryConfig;
use DigiMonks\DataTables\Queries\DBALQuery;
use Doctrine\DBAL\Query\QueryBuilder;
use DigiMonks\Form\Element\Input;
use DigiMonks\Form\Form;
use DigiMonks\SymfonyBootstrap\Form\FormBuilder;
use DigiMonks\SymfonyBootstrap\Presenter\AbstractPresenter;
use DigiMonks\SymfonyBootstrap\FormHandler\DoctrineFormHandler;
use DigiMonks\SymfonyBootstrap\FormPopulator\DoctrineFormPopulator;
use Psr\Http\Message\ServerRequestInterface;

class SitePresenter extends AbstractPresenter
{
    const MENU_ICON = 'fad fa-browser';

    const MENU_LABEL = 'Sites';

    // const MENU_LINK = '#';

    /**
    * Used to mark the  menu-item as active
    * if the user opens the site form
    */
    // const FORM_LINK = '/cp/form/site/*';

    /**
    * @param ServerRequestInterface $request
    * @return DataTable
    */
    protected function makeDataTable(ServerRequestInterface $request): DataTable
    {
        // check permissions
        $this->checkPermissions('Site', 'read');

        if ($this->isGranted('create.Site')) {
            $this->viewData->setCreateButton('<a href="' . backend_url('/form/site') . '" class="btn-blue-light btn-floating" data-tooltip="New site" data-position="left"><span class="fal fa-plus"></span></a>');
        }

        $this->viewData->setPageTitle('Sites');

        $dataTable = DataTable::make(null, $request)
            ->setHeadingIcon(sprintf('<span class="dt-header-icon"><span class="%s"></span></span>', self::MENU_ICON))
            ->setHeading('<h1 class="dt-header-title">Sites</h1>')
            ->setWrapperClass('do-collection')
            ->query(new class ($this->getDoctrine()->getConnection(), 'site') extends DBALQuery {
                protected function createQueryBuilder(DataTableQueryConfig $queryConfig): QueryBuilder
                {
                    return parent::createQueryBuilder($queryConfig);
                }
            })
            ->link(function (array $record) {
                return $this->generateUrl('form', ['entityName' => 'site', 'guid' => $record['site.guid']]);
            })
            ->add(Column::make('site.url'))
            ->add(Column::make('site.created_at', function($record) {
                return (new \DateTime($record['site.created_at']))->format("d-m-Y H:i");
            }))
            ->add(Column::make('site.days_left'))
            ->add(Column::make('site.guid')->setHidden(true))
            ->buttons(
                \DigiMonks\DataTables\Button::make(function (array $record) {
                    return $this->generateUrl('form', ['entityName' => 'site', 'guid' => $record['site.guid']]);
                })
                ->setIcon('fal fa-pencil')
                ->tooltip('Edit site')
            )
            ;

        return $dataTable;
    }


    /**
    * @param ServerRequestInterface $request
    * @param null|string $guid
    * @return Form
    */
    protected function makeForm(ServerRequestInterface $request, $guid = null): Form
    {
        // check permissions
        // $this->denyAccessUnlessGranted("super-admin");

        $this->viewData->setTitle(sprintf('<span class="dm-form-header-icon"><span class="%s"></span></span> create site', self::MENU_ICON));

        $siteRepository = $this->getDoctrine()->getRepository(Site::class);
        $site = $siteRepository->findOneBy(['guid' => $guid]) ?? new Site();
        $this->setModel($site);
        if ($site && $site->getId()) {
            $this->viewData->setTitle(sprintf('<span class="dm-form-header-icon"><span class="%s"></span></span> update site', self::MENU_ICON));
            $this->setNextButton($site);
            $this->setPrevButton($site);
        }

        $formBuilder = FormBuilder::make('site_form', true, new DoctrineFormPopulator())
            ->ajax()
            ->addHandler(
                new DoctrineFormHandler(
                    $this->getDoctrine()->getManager(),
                    [
                        'created_by' => $this->getUser()->getId(),
                        'updated_by' => $this->getUser()->getId(),
                    ]
                )
            )
            ->setModel($site)
            ->add(Input::make("url", "URL")->addRule('required'))
            ->newLine();

        // @todo check create permissions
        $this->createButtonUrl = backend_url('/form/site');

        return $formBuilder
            ->build($request);
    }

}

<?php

namespace App\Presenter;

use App\Entity\Check;
use App\Entity\Notification;
use DigiMonks\DataTables\Column;
use DigiMonks\DataTables\DataTable;
use DigiMonks\DataTables\DataTableQueryConfig;
use DigiMonks\DataTables\Queries\DBALQuery;
use DigiMonks\Form\Element\CodeEditor;
use DigiMonks\Form\Element\Hidden;
use DigiMonks\Form\Element\Relation;
use DigiMonks\Form\Element\Textarea;
use Doctrine\DBAL\Query\QueryBuilder;
use DigiMonks\Form\Element\Input;
use DigiMonks\Form\Form;
use DigiMonks\SymfonyBootstrap\Form\FormBuilder;
use DigiMonks\SymfonyBootstrap\Presenter\AbstractPresenter;
use DigiMonks\SymfonyBootstrap\FormHandler\DoctrineFormHandler;
use DigiMonks\SymfonyBootstrap\FormPopulator\DoctrineFormPopulator;
use Psr\Http\Message\ServerRequestInterface;

class NotificationPresenter extends AbstractPresenter
{
    const MENU_ICON = 'fal fa-bells';

    const MENU_LABEL = 'Notifications';

    // const MENU_LINK = '#';

    /**
     * Used to mark the  menu-item as active
     * if the user opens the notification form
     */
    // const FORM_LINK = '/cp/form/notification/*';

    /**
     * @param ServerRequestInterface $request
     * @return DataTable
     */
    protected function makeDataTable(ServerRequestInterface $request): DataTable
    {
        // check permissions
        $this->checkPermissions('Notification', 'read');

        if ($this->isGranted('create.Notification')) {
            $this->viewData->setCreateButton('<a href="' . backend_url('/form/notification') . '" class="btn-blue-light btn-floating" data-tooltip="New notification" data-position="left"><span class="fal fa-plus"></span></a>');
        }

        $this->viewData->setPageTitle('Notifications');

        $dataTable = DataTable::make(null, $request)
            ->setHeadingIcon(sprintf('<span class="dt-header-icon"><span class="%s"></span></span>', self::MENU_ICON))
            ->setHeading('<h1 class="dt-header-title">Notifications</h1>')
            ->setWrapperClass('do-collection')
            ->query(new class ($this->getDoctrine()->getConnection(), 'notification') extends DBALQuery {
//                protected function createQueryBuilder(DataTableQueryConfig $queryConfig): QueryBuilder
//                {
//                    return parent::createQueryBuilder($queryConfig)
//                        ->leftJoin('notification', 'relationTable', 'relationAlias', 'notification.product_id = relationAlias.id');
//                }
            })
            ->link(function (array $record) {
                return $this->generateUrl('form', ['entityName' => 'notification', 'guid' => $record['notification.guid']]);
            })
            ->add(Column::make('notification.subject')->setSortable(true)->setSearchable(true))
            ->add(Column::make('notification.sent_at')->setSortable(true)->setDateFormat('H:i'))
           ->add(Column::make('checks', function (array $record) {
               $notification = $this->getDoctrine()->getRepository(Notification::class)->findOneBy(['guid' => $record['notification.guid']]);
               $checks = [];
               foreach($notification->getChecks() as $check) {
                   $checks[] = $check->getDescription();
               }
               return "<ol class='list-p-reset list-m-reset'><li>" . implode('</li><li>', $checks) . "</li></ol>";
           })->setOmitFromQuery(true)->small())
            ->add(Column::make('notification.recipients'))
            ->add(Column::make('notification.guid')->setHidden(true))
            ->buttons(
                \DigiMonks\DataTables\Button::make(function (array $record) {
                    return $this->generateUrl('form', ['entityName' => 'notification', 'guid' => $record['notification.guid']]);
                })
                    ->setIcon('fal fa-pencil')
                    ->tooltip('Edit notification')
            );

        return $dataTable;
    }


    /**
     * @param ServerRequestInterface $request
     * @param null|string $guid
     * @return Form
     */
    protected function makeForm(ServerRequestInterface $request, $guid = null): Form
    {
        // check permissions
        // $this->denyAccessUnlessGranted("super-admin");

        $this->viewData->setTitle(sprintf('<span class="dm-form-header-icon"><span class="%s"></span></span> create notification', self::MENU_ICON));

        $notificationRepository = $this->getDoctrine()->getRepository(Notification::class);
        $notification = $notificationRepository->findOneBy(['guid' => $guid]) ?? new Notification();
        $this->setModel($notification);
        if ($notification && $notification->getId()) {
            $this->viewData->setTitle(sprintf('<span class="dm-form-header-icon"><span class="%s"></span></span> update notification', self::MENU_ICON));
            $this->setNextButton($notification);
            $this->setPrevButton($notification);
        }


        $formBuilder = FormBuilder::make('notification_form', true, new DoctrineFormPopulator())
            ->ajax()
            ->addHandler(
                new DoctrineFormHandler(
                    $this->getDoctrine()->getManager(),
                    [
                        'created_by' => $this->getUser()->getId(),
                        'updated_by' => $this->getUser()->getId(),
                    ]
                )
            )
            ->setModel($notification)
            ->add(Input::make("subject")->addRule('required'))
            ->add(Relation::make('checks', Check::class, $this->getDoctrine()->getRepository(Check::class)->pluck('description', 'id'))->setMultiple(true))
            ->add(Input::make('sentAt')->setType('time')->setValue($notification->getSentAt() ? $notification->getSentAt()->format('H:i') : null))
            ->newLine()
            ->add(Textarea::make("bodyIntro"))
            ->add(TextArea::make("recipients")->setHelperText("Comma seperated")->addRule('required'))
        ;

//            ->newLine()


        // @todo check create permissions
        $this->createButtonUrl = backend_url('/form/notification');

        return $formBuilder
            ->build($request);
    }

}

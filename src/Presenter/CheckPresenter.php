<?php

namespace App\Presenter;

use App\Entity\Check;
use App\Entity\Notification;
use DigiMonks\DataTables\Column;
use DigiMonks\DataTables\DataTable;
use DigiMonks\DataTables\DataTableQueryConfig;
use DigiMonks\DataTables\Queries\DBALQuery;
use DigiMonks\Form\Element\DateTime;
use DigiMonks\Form\Element\Number;
use DigiMonks\Form\Element\Relation;
use DigiMonks\Form\Element\Select;
use Doctrine\DBAL\Query\QueryBuilder;
use DigiMonks\Form\Element\Input;
use DigiMonks\Form\Form;
use DigiMonks\SymfonyBootstrap\Form\FormBuilder;
use DigiMonks\SymfonyBootstrap\Presenter\AbstractPresenter;
use DigiMonks\SymfonyBootstrap\FormHandler\DoctrineFormHandler;
use DigiMonks\SymfonyBootstrap\FormPopulator\DoctrineFormPopulator;
use Psr\Http\Message\ServerRequestInterface;

class CheckPresenter extends AbstractPresenter
{
    const MENU_ICON = 'fal fa-check-double';

    const MENU_LABEL = 'Checks';

    // const MENU_LINK = '#';

    /**
    * Used to mark the  menu-item as active
    * if the user opens the check form
    */
    // const FORM_LINK = '/cp/form/check/*';

    /**
    * @param ServerRequestInterface $request
    * @return DataTable
    */
    protected function makeDataTable(ServerRequestInterface $request): DataTable
    {
        // check permissions
        $this->checkPermissions('Check', 'read');

        if ($this->isGranted('create.Check')) {
            $this->viewData->setCreateButton('<a href="' . backend_url('/form/check') . '" class="btn-blue-light btn-floating" data-tooltip="New check" data-position="left"><span class="fal fa-plus"></span></a>');
        }

        $this->viewData->setPageTitle('Checks');

        $dataTable = DataTable::make(null, $request)
            ->setHeadingIcon(sprintf('<span class="dt-header-icon"><span class="%s"></span></span>', self::MENU_ICON))
            ->setHeading('<h1 class="dt-header-title">Checks</h1>')
            ->setWrapperClass('do-collection')
            ->query(new class ($this->getDoctrine()->getConnection(), '`check`') extends DBALQuery {
//                protected function createQueryBuilder(DataTableQueryConfig $queryConfig): QueryBuilder
//                {
//                    return parent::createQueryBuilder($queryConfig)
//                    ->leftJoin('check', 'relationTable', 'relationAlias', 'check.product_id = relationAlias.id');
//                }
            })
            ->link(function (array $record) {
                return $this->generateUrl('form', ['entityName' => 'check', 'guid' => $record['check.guid']]);
            })
            ->add(Column::make('check.description')->setSearchable(true)->setSortable(true))
            ->add(Column::make('check.type')->setSearchable(true)->setSortable(true))
            ->add(Column::make('check.time_span')->setSortable(true)->setDateFormat('H:i'))

            ->add(Column::make('check.guid')->setHidden(true))
            ->buttons(
                \DigiMonks\DataTables\Button::make(function (array $record) {
                    return $this->generateUrl('form', ['entityName' => 'check', 'guid' => $record['check.guid']]);
                })
                ->setIcon('fal fa-pencil')
                ->tooltip('Edit check')
            )
            ;

        return $dataTable;
    }


    /**
    * @param ServerRequestInterface $request
    * @param null|string $guid
    * @return Form
    */
    protected function makeForm(ServerRequestInterface $request, $guid = null): Form
    {
        // check permissions
        // $this->denyAccessUnlessGranted("super-admin");

        $this->viewData->setTitle(sprintf('<span class="dm-form-header-icon"><span class="%s"></span></span> create check', self::MENU_ICON));

        $checkRepository = $this->getDoctrine()->getRepository(Check::class);
        $check = $checkRepository->findOneBy(['guid' => $guid]) ?? new Check();
        $this->setModel($check);
        if ($check && $check->getId()) {
            $this->viewData->setTitle(sprintf('<span class="dm-form-header-icon"><span class="%s"></span></span> update check', self::MENU_ICON));
            $this->setNextButton($check);
            $this->setPrevButton($check);
        }

        $this->addEncoreEntryPoints('check_form');

        $formBuilder = FormBuilder::make('check_form', true, new DoctrineFormPopulator())
            ->ajax()
            ->addHandler(
                new DoctrineFormHandler(
                    $this->getDoctrine()->getManager(),
                    [
                        'created_by' => $this->getUser()->getId(),
                        'updated_by' => $this->getUser()->getId(),
                    ]
                )
            )
            ->setModel($check)
            ->add(Input::make('description'))
            ->newLine()
            ->add(Select::make('type', Check::TYPES)->setId('typeSelect')->addRule('required'))
            ->add(Input::make('timeSpan')->setType('time')->setValue($check->getTimeSpan() ? $check->getTimeSpan()->format('H:i') : null))
            ->add(Input::make('message'))
            ->add(Select::make('level', Check::LEVELS)->setDefaultValue('none'))
            ->newLine(['class' => 'elastic-log-input'])
            ->add(Input::make('elasticIndex')->setHelperText("Enter index name without the date for example: 'app-jaap-dev-'"))
            ->add(Number::make('min')->setId('minCount')->setHelperText("If no min set to 0"))->columnId('minCountColumn')
            ->add(Number::make('max')->setId('maxCount')->setHelperText("If no max set to 0"))->columnId('maxCountColumn')
            ->newLine();

        // @todo check create permissions
        $this->createButtonUrl = backend_url('/form/check');

        return $formBuilder
            ->build($request);
    }

}

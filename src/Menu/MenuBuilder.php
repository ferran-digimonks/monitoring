<?php

namespace App\Menu;

use App\Entity\Check;
use App\Entity\Notification;
use App\Entity\Site;
use App\Entity\User;

use DigiMonks\SymfonyBootstrap\Entity\MailTemplate;
use DigiMonks\SymfonyBootstrap\Entity\Translation;
use DigiMonks\SymfonyBootstrap\Gui\Menu\Menu;
use DigiMonks\SymfonyBootstrap\Gui\Menu\MenuBuilderInterface;
use DigiMonks\SymfonyBootstrap\Gui\Menu\MenuFactoryInterface;
use DigiMonks\SymfonyBootstrap\Gui\Menu\MenuItem;
use DigiMonks\SymfonyTenants\Context\TenantContext;
use Symfony\Component\Security\Core\Security;
use DigiMonks\SymfonyTenants\Entity\Role;
use DigiMonks\SymfonyTenants\Entity\Tenant;

class  MenuBuilder implements MenuBuilderInterface
{
    /**
    * @var Security
    */
    private $security;

    /**
    * @var MenuFactoryInterface
    */
    private $factory;

    /**
    * @var TenantContext
    */
    private $tenantContext;


    public function __construct(Security $security, MenuFactoryInterface $factory, TenantContext $tenantContext)
    {
        $this->security = $security;
        $this->factory = $factory;
        $this->tenantContext = $tenantContext;
    }

    public function sidebar(): Menu
    {
        $user = $this->security->getUser();

        if ($this->tenantContext->isTenantRole()) {
            return $this->factory
                ->create()
                ->add(
                    Site::class,
                    Check::class,
                    Notification::class
                )
                ->add(User::class)
                ->getMenu();
        } else {
            return $this->factory
                ->create()
                ->add(
                    User::class,
                    Tenant::class,
                    Role::class,
                    MailTemplate::class,
                    Translation::class
                )
                ->getMenu();
        }

    }

    public function topbar(): Menu
    {
        return $this->factory
            ->create()
            //            ->add(
            //                MenuItem::make('Dashboard', '/dashboard')
            //                    ->setIcon('fal fa-star')
            //                    ->addMenuItem(MenuItem::make('some subpage', '/'))
            //            )
            //            ->add(User::class)
            ->getMenu();
    }
}
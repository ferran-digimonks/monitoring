# @see https://github.com/nicwortel/symfony-skeleton/blob/master/Makefile

test: phpstan phpunit test_forms

install:
	ln -s ../../pre-commit.sh .git/hooks/pre-commit

update:
	composer update
	git submodule foreach git pull
	bin/console make:migration
	bin/console dm:publish
	bin/console assets:install
	yarn && yarn build

phpunit:
	./bin/phpunit --colors=always

test_forms:
	./bin/phpunit digimonks/form/tests --colors=always

phpstan:
	./vendor/bin/phpstan analyse --level=7 --memory-limit=1g src tests

code_beautify_and_fix:
	./vendor/bin/phpcbf

code_standards:
	./vendor/bin/phpcs

code_fix_and_check_standards: code_beautify_and_fix code_standards

composer-validate:
	composer validate --no-check-publish

# This will compare the timestamp of the vendor directory with those of composer.json and composer.lock,
# and will only execute the recipe if either of the composer files is newer than the vendor directory, or if the vendor directory is missing.
# If you are using a Makefile for a library where the composer.lock is not committed, you can use a wildcard so Make won't fail if composer.lock does not exist:
# vendor: composer.json $(wildcard composer.lock)
vendor: composer.json composer.lock
	composer install
